# frozen_string_literal: true

require "uri"

module Devise
  module Controllers
    # Provide the ability to store a location.
    # Used to redirect back to a desired path after sign in.
    # Included by default in all controllers.
    module StoreLocation
      # Returns and delete (if it's navigational format) the url stored in the session for
      # the given scope. Useful for giving redirect backs after sign up:
      #
      # Example:
      #
      #   redirect_to stored_location_for(:user) || root_path
      #
      def stored_location_for(resource_or_scope)
        print("[Store Location] Reading stored location: " + resource_or_scope.to_s)
        session_key = stored_location_key_for(resource_or_scope)

        if not session[session_key]
          print("[Store Location] No locations stored")
          return nil
        end


        locations = session[session_key].split("\n")
        location = locations[-1]

        if is_navigational_format?
          print("[Store Location] Deleting location")
          session[session_key] = locations.slice(0, locations.length - 1).join("\n")
        end

        print("[Store Location] Location found: " + location)
        location
      end

      # Stores the provided location to redirect the user after signing in.
      # Useful in combination with the `stored_location_for` helper.
      #
      # Example:
      #
      #   store_location_for(:user, dashboard_path)
      #   redirect_to user_facebook_omniauth_authorize_path
      #
      def store_location_for(resource_or_scope, location)
        session_key = stored_location_key_for(resource_or_scope)

        print("[Store Location] Storing location for " + resource_or_scope.to_s + " (" + location + ") at " + session_key + "\n")
        
        path = extract_path_from_location(location)
        session[session_key] = "" unless session[session_key]
        session[session_key] = session[session_key] + "\n" + path if path
      end

      private

      def parse_uri(location)
        location && URI.parse(location)
      rescue URI::InvalidURIError
        nil
      end

      def stored_location_key_for(resource_or_scope)
        scope = Devise::Mapping.find_scope!(resource_or_scope)
        "#{scope}_return_to"
      end

      def extract_path_from_location(location)
        uri = parse_uri(location)

        if uri 
          path = remove_domain_from_uri(uri)
          path = add_fragment_back_to_path(uri, path)

          path
        end
      end

      def remove_domain_from_uri(uri)
        [uri.path.sub(/\A\/+/, '/'), uri.query].compact.join('?')
      end

      def add_fragment_back_to_path(uri, path)
        [path, uri.fragment].compact.join('#')
      end
    end
  end
end
